#!/usr/bin/env python3

import subprocess
import logging
import datetime
import ipaddress

logging.basicConfig(filename='app.log', filemode='a', format='%(asctime)s - %(message)s')

def __read_file():
    with open('/home/95ixvc/stuff/wulug/workshop/list_ip.txt') as file:
        for line in file:
            lines = line.strip()
            __check_if_up(lines)

def __check_if_up(ip):
    #ping_return = subprocess.call('ping -c 1 google.coam', shell = True)
    print(ip)
    ping_return = subprocess.call('ping -c 2 ' + ip, shell = True)

    # nc -zv ip port(443 or 80) works too
    if ping_return is not 0:
        # datetime_now = datetime.datetime.now()
        # logging.critical('IP is down at {}'.format(datetime_now))
        logging.critical('System Down at {}'.format(ip))

def main():
    __read_file()

if __name__ == "__main__":
    main()
